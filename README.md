#Neuralnetworksanddeeplearning

This is a repository for sharing work and solutions when collaborating on the book Neuralnetworksanddeeplearning.com http://neuralnetworksanddeeplearning.com/

Jupyter can be useful for playing around with Python code
To run Jupyter notebooks follow instructions in this (quick guide)[https://jupyter-notebook-beginner-guide.readthedocs.io/en/latest/]
Notebooks should be located within chapter folders
